.phony: data clean


exec: TP2.o Valider.o
	gcc -Wall -pedantic -std=c99  -o TP2 TP2.o Valider.o -lpthread

TP2.o: TP2.c
	gcc -Wall -pedantic -std=c99  -c TP2.c

Valider.o: Valider.c
	gcc -Wall -pedantic -std=c99  -c Valider.c

test :
	./TP2

clean:
	rm *.o
	rm TP2
