#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <time.h>

#define NOMBRE_DE_PHILOSOPHES 5
#define BAGUETTES 5

void *actionDuPhilosophe (void *id);
void bloquerBaguette (int philosophe, int baguette);
void libererBaguette (int baguetteDeGauche, int baguetteDeDroite);
void affichage (int philosophe, int code, char etat[]);

pthread_mutex_t baguettes[BAGUETTES];
pthread_t philos[NOMBRE_DE_PHILOSOPHES];
pthread_cond_t baguettePrete = PTHREAD_COND_INITIALIZER;
int tempsDeRepos = 3;

typedef struct Philosophe Philosophe;
struct Philosophe {

	int code; //0..49
	//char nom[15];
	//char action[15];
};
