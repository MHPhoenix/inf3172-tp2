#include "TP2.h"
#include "Valider.h"

Philosophe code; 
int main ( int argc, char *argv[] ) {

	pthread_cond_init (&baguettePrete, NULL);
	FILE* fichier = NULL;

	//cree et initialise les verrous (mutex) baguettes
	for (int i = 0; i < BAGUETTES; i++) {
		pthread_mutex_init (&baguettes[i], NULL);
	}

	//Pour affichage de l'entete de resultat.txt
	//w+: car nettoie le fichier au prealable
	fichier = fopen ("resultat.txt", "w+");
        if (fichier != NULL) {
                fprintf (fichier, "Code 			Nom			Action\n\n");
        } else {
                printf ("Impossible d'ouvrir le fichier resultat.txt");
        }

	// creation de 5 threads philosophes
	for (int i = 0; i < NOMBRE_DE_PHILOSOPHES; i++) {
		pthread_create (&philos[i], NULL, actionDuPhilosophe, (void *)i);
	}

	// Fermer le fichier
	fclose (fichier);

	// Attendre que le thread finisse
	for (int i = 0; i < NOMBRE_DE_PHILOSOPHES; i++) {
                pthread_join (philos[i], NULL);
        }

	//Detruire le mutex a la fin
	for (int i = 0; i < NOMBRE_DE_PHILOSOPHES; i++) {
                pthread_mutex_destroy (&baguettes[i]);
        }

	option ();

	return 0;
}


void *actionDuPhilosophe (void *numPhilo) {

	int numDuPhilo = 0, baguetteDeDroite = 0, baguetteDeGauche = 0;
	// initialisation de rand
	char etat [6] = "";

	while (code.code >= 0 && code.code <= 20)
	{
    	numDuPhilo = (int)numPhilo;
		numDuPhilo = rand() %  NOMBRE_DE_PHILOSOPHES;

		/**
	 	* Vue que c'est une table ronde, le voisin de gauche
	 	* du 1er philosophe sera le dernier philosophe (4e)
		**/
		if (numDuPhilo == 0) {
			baguetteDeGauche = 4;
		} else {
			baguetteDeGauche = numDuPhilo - 1;
			baguetteDeDroite = numDuPhilo;
		}

		/**
	 	* Chaque philosophe cherche d'abord sa propre
	 	* baguette qui est celle avec son numero.
	 	* Quand il a sa baguette assignee, il atteint
		* la baguette assignee a son voisin
		**/
		bloquerBaguette (numDuPhilo, baguetteDeGauche);
		bloquerBaguette (numDuPhilo, baguetteDeDroite);

		code.code += 1;

		//si le philosophe a pris les deux
		//baguettes alors il mange
		if (numDuPhilo == 0) {

			strcpy (etat, "mange");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 1) {

	   		strcpy (etat, "mange");
	   		affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 2) {

			strcpy (etat, "mange");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 3) {

			strcpy (etat, "mange");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 4) {

			strcpy (etat, "mange");
	    	affichage (numDuPhilo, code.code, etat);
		}

		//Mange pendant 3 secondes
		sleep (tempsDeRepos);

		libererBaguette (baguetteDeGauche, baguetteDeDroite);

		//Quand les deux baguettes sont liberees
		//le philosophe pense
		if (numDuPhilo == 0) {

			strcpy (etat, "pense");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 1) {

	   		strcpy (etat, "pense");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 2) {

	   		strcpy (etat, "pense");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 3) {

			strcpy (etat, "pense");
	    	affichage (numDuPhilo, code.code, etat);

		} else if (numDuPhilo == 4) {
	   		strcpy (etat, "pense");
	    	affichage (numDuPhilo, code.code, etat);
		}
	}
	return NULL;
}


/**
* Bloque une baguette qui est prise
*
**/
void bloquerBaguette (int philosophe, int baguette) {
	pthread_mutex_lock (&baguettes[baguette]);
}


/**
* Libere une baguette qui est n'est plus utilise
*
**/
void libererBaguette (int baguetteDeGauche, int baguetteDeDroite) {
    pthread_mutex_unlock (&baguettes[baguetteDeGauche]);
	pthread_mutex_unlock (&baguettes[baguetteDeDroite]);
}

/**
* Affiche les differentes actions (mange, pense)
* dans le fichier resultat
**/
void affichage (int philosophe, int code, char etat[]) {

	FILE* fichier = NULL;

	// a: car ajoute du contenu a la fin du fichier
	fichier = fopen ("resultat.txt", "a");

	if (fichier != NULL) {
		fprintf (fichier, "%d			Philosophe %d		%s\n\n", code, philosophe, etat);
	} else {
		printf ("Impossible d'ouvrir le fichier resultat.txt");
	}
}

