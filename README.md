# TP2 OS


 ## Toutes les fonctions de compilation se trouvent dans un fichier nomme Makefile
 ## Avant de compiler ce programme il faut s'assurer, en premier lieu de supprimer tous les fichiers objets en faisant make clean.

 ## Pour compiler il suffit d'ecrire make dans le terminal.

 ## Pour l'affichage des resultats, il faut  ecrire  make test dans le terminal.

 ## Pour que le programme compile, il faut s'assurer de la presence de tous les fichiers.
   A savoir :

	> . TP2.c
	> . TP2.h
	> . Valider.c
	> . Valider.h
	> . resultat.txt (qui est un fichier vide) et sera remplit a la compilation.
	> . Makefile

 ## AUTEURS

	### MONGBO Houefa Orphyse Peggy Merveille (MONH08519906)
	
	### Lisa Steccy Kaneza (KANL30589606)

	### Yasmine KAID (KAIY21559708)
